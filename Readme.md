# Loki et FluentD

 1. [Loki](#Loki)
 2. [Fluentd](#Fluentd)
 3. [La stack EFK vs la stack LFG](#Comparaison)
 4. [Exemple dans un cluster kubernetes avec tomcat](#Exemple)

---

## Loki 

[Repo Github](https://github.com/grafana/loki)

Comme le designe les developpeurs eux-mêmes, Loki est comme prometheus mais pour les logs. 
Loki est donc un systeme d'aggregation de logs. Mais, au lieu d'indexer les logs, loki va indexer les logs "en vrac" et seulement indexer les meta-données de ces logs. De la même facon que prometheus, loki va utiliser des labels.
Cette particularité fait de Loki un outil consommant relativement peu de ressources et nativement adapté pour recuperer les logs de pods puisque lorsqu'on va lui donner des logs de pods, il va automatiquement indexer les label kubernetes.


![Architecture standard d'une stack PLG](https://grafana.com/static/assets/img/blog/image1.png)
Voici l'architecture standard de la stack telle que pensée par les developpeurs de loki. Cependant, comme on le verra dans l'exemple, on peut très bien remplacer promtail par nimporte quel outil capable de forwarder des logs.

Cependant, encore une fois selon leur propres mots, l'objectif principal qui a motivé et guidé le developpement était la possibilité de faire rapidement et facilement un grep sur les logs. On se retrouve donc dans une situation où, par exemple, on ne peut pas faire aujourd'hui d'alerting (bien que ce soit une feature qu'ils veulent implementer à l'avenir).

[Lien vers la documentation Loki](https://github.com/grafana/loki/tree/master/docs)


## Fluentd
[Repo GitHub](https://github.com/fluent/fluentd)

Fluentd est un outil de collection de données porté par la Cloud Native Computing Foundation. 
Fluentd est extremement polyvalent. La où nous allons seulement l'utiliser pour centraliser et parser des logs, il est capable de faire de lui même de l'alerting ou meme du logging d'evenements ainsi que bien d'autres usecases que je ne vais pas lister ici.
Un des objectifs de fluentd est d'atteindre le "[Unified Logging Layer](https://www.fluentd.org/blog/unified-logging-layer)".  C'est à dire qu'il est capable de récuperer des logs de nimporte quels types et est capable de les transmettre a à peu près nimportequel outil. En effet, un des points forts de Fluentd est [le nombre de plugins](https://www.fluentd.org/plugins/all) pour prendre en charge différentes sources de données et différentes sorties.
Fluentd est également un outil relativement léger pour les taches qu'il réalise ( Dans l'exemple qui suit, nous verrons qu'il fonctionne très bien avec une limite de 200mi (~210 mo)), sur le site officiel ils parlent de 40-50mo minimum.


## Comparaison

Une comparaison à été faite par les developpeurs de loki [ici](https://github.com/grafana/loki/blob/master/docs/overview/comparisons.md).

En quelques points: 
 - Loki etant un outil très recent, on trouve trop peu de documentation et d'exemples incluant celui-ci
 - Au niveau des performances, la stack EFK consomme bien plus de ressources que la stack LPG (Ou la LFG que nous allons utiliser dans l'exemple) mais en contrepartie offre bien plus de fonctionnalitée. Loki ne permet pour l'instant que de faire des query simples sur les logs. 
 - Pour un utilisateur n'ayant aucune experience sur aucun des deux outils, la stack EFK sera plus compliquée à prendre en main.

En conclusion, je penses qu'il est très bien de se tenir a ce qui est dit dans [cet article](https://grafana.com/blog/2018/12/12/loki-prometheus-inspired-open-source-logging-for-cloud-natives/). Si on veut une solution simple à utiliser et maintenir uniquement dans le but de centraliser les logs et de pouvoir les query alors LPG/LFG est probablemnt le choix à faire mais dès lors que l'on a besoin d'integrer de l'alerting, des visualisations, etc... il vaut mieux utiliser la stack EFK.
Si on doit parler de ce qui peux venir s'ajouter a chacunes des deux stacks, pour EFK on va donc avoir tous les logiciels beats et APM. Pour LPG/LFG on va avoir Prometheus et les plugins Grafana.

## Exemple

Prérequis: Un cluster k8s avec helm
/!\ cet exemple à été réalisé avec helm 2.14
On commence par cloner le repo.

On Crée le service account qui sera utilisé par fluentd

```kubectl apply -f fluentd-sa.yaml```

On crée le namespace monitoring

```kubectl create namespace monitoring```

Pour cet exemple, nous avons un daemonset qui va run Fluentd sur chaque node de notre cluster.
L'exemple le plus courant que l'on peut trouver en faisant des recherches va etre d'avoir un daemonset Fluent-bit qui va forward les données vers une instance FluentD.

On installe prometheus et grafana avec helm

    helm install stable/prometheus-operator \  
	--name prom-op \  
	--namespace monitoring \  
	--set prometheus.service.type=NodePort \  
	--set grafana.service.type=NodePort \  
	--set commonLabels.prometheus=prom-op \  
	--set prometheus.prometheusSpec.serviceMonitorSelector.matchLabels.prometheus=prom-op

On installe maintenant loki

    helm repo add loki [https://grafana.github.io/loki/charts](https://grafana.github.io/loki/charts)  
	helm repo update  
	helm install loki/loki --name loki --namespace monitoring


Dans le daemonset, on indique des Volumes sur les répertoires /var/log et /var/lib/docker/containers des nodes. On à également ajouté une commande pour installer le plugin loki dans fluentd.

Au niveau de la Configmap, on va definir le contenu du fluentd.conf . Etant donné que l'on est sur des fichiers de logs on indique en source qu'on est en type "tail". Pour les logs java,  on utilise le mode de format "Multiline" avec une Regex afin que la stacktrace soit considérée comme un seul log lorsque l'on va faire une query avec loki. Et enfin dans le dernier bloc, on indique qu'on va utiliser un loki comme un storage.  On va aussi pouvoir ajouter dans la ligne "label_keys" les labels que loki va indexer.

    kubectl create -f configmap.yaml -f daemonset.yaml

Si besoin on déploie le loadbalancer pour acceder à grafana

    kubectl apply -f grafanalb.yaml
    
On deploie maintenant notre conteneur (L'image utilisée ici crache des logs a intervalles reguliers)

    kubectl apply -f deployment.yaml

On peut maintenant se rendre sur l'ip de notre instance grafana et s'identifier (admin:prom-operator). On va ajouter notre data source loki dans grafana avec l'url  `http://loki:3100`.

Pour voir nos logs, on va aller dans la partie "explore" de grafana et selectionner Loki dans le menu deroulant tout en haut. On peut soit utiliser le menu deroulant "Log labels" pour generer la query soit la faire soit meme. Ici on va ecrire la query `{app="tomcat"}` pour voir les logs correspondants à ce label.


Plus d'infos: 

 - https://grafana.com/blog/2018/12/12/loki-prometheus-inspired-open-source-logging-for-cloud-natives/
 - https://github.com/grafana/loki/blob/master/docs/overview/comparisons.md

Lien utile pour la conf fluentd:
 - https://github.com/GoogleCloudPlatform/fluentd-catch-all-config/tree/master/configs/config.d








Comment se passe le scaling de loki ?  https://github.com/grafana/loki/blob/master/docs/operations/scalability.md
Stockage de loki  https://github.com/grafana/loki/blob/master/docs/operations/storage/README.md

Qu'est ce qui se passe si fluentd/loki est down ? https://docs.fluentd.org/deployment/failure-scenarios



Merci a Malo Dumont pour l'image java